package pl.sevay.desktop;

import pl.sevay.main.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Czolgi - Projekt zespolowy";
		new LwjglApplication(new Game(), config);
	}
}
