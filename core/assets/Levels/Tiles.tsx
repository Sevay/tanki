<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.4" name="Tiles" tilewidth="16" tileheight="16" tilecount="16" columns="8">
 <image source="SpriteSheet.png" width="128" height="32"/>
 <tile id="0">
  <properties>
   <property name="Collidable" value="True"/>
   <property name="Eagle" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="Collidable" value="True"/>
   <property name="Eagle" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="Collidable" value="True"/>
   <property name="Destructible" value="True"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="Spawnable" value="True"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="Collidable" value="True"/>
   <property name="Eagle" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="Collidable" value="True"/>
   <property name="Eagle" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="Collidable" value="True"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="Collidable" value="True"/>
   <property name="bulletDestroy" value="True"/>
  </properties>
 </tile>
</tileset>
