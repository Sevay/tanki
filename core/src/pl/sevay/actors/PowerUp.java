package pl.sevay.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

public class PowerUp extends GameObject {
	private boolean alive;
	private int type;
	public int time;
	private Random random;
	private Texture spriteSheet;
	private Vector2 position;
	private int spriteSheetRows;
	private int spriteSheetCols;
	private int numFrames;

	public static final int BONUS = 0;
	public static final int KILL_ALL_ENEMIES_ON_SCREEN = 1;
	public static final int STOP_ENEMIES = 2;

	public PowerUp(Texture spriteSheet, Vector2 position, int spriteSheetRows,
                   int spriteSheetCols, int numFrames, int type) {
		super(spriteSheet, position, spriteSheetRows, spriteSheetCols, numFrames, type);
		random = new Random();
		this.spriteSheet = spriteSheet;
		this.position = position;
		this.spriteSheetRows = spriteSheetRows;
		this.spriteSheetCols = spriteSheetCols;
		this.numFrames = numFrames;
		this.type = type;
		time = random.nextInt(200)+360;
		alive = false;
	}

	public int getType() {
		return type;
	}

	public PowerUp colisionSetup() {
		setUp();
		return new PowerUp(spriteSheet, this.position, this.spriteSheetRows, this.spriteSheetCols, this.numFrames, this.type);
	}
	
	@Override public void update(float dt){
		if(time > 0) {
			time--;
		} else {
			alive = true;
		}
		super.update(dt);
	}

	private void setUp() {
		type = random.nextInt(3);
		alive = false;
		setPosition(random.nextInt(350)+15, random.nextInt(350)+15);
	}

	private void setPosition(int x, int y) {
		position.set(x, y);
	}
	
	@Override public void draw(SpriteBatch batch){
		if(isAlive()) {
			super.draw(batch);
		}
	}

	public Vector2 getPosition() {
		return this.position;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}
}
