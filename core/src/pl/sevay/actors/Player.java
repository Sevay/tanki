package pl.sevay.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class Player extends GameObject {
	private boolean alive;

	
	public Player(Texture spriteSheet, Vector2 position, int spriteSheetRows,
			int spriteSheetCols, int numFrames, int animationStartRow){		
		super(spriteSheet, position, spriteSheetRows, spriteSheetCols, numFrames, animationStartRow);
		alive = true;
	}
	
	@Override public void update(float dt){
		
		super.update(dt);
	}

	public void setPosition(int x, int y) {
		position.set(x, y);
	}
	public Vector2 getPosition() { return position; }
	
	@Override public void draw(SpriteBatch batch){
		if(isAlive()) {
			super.draw(batch);
		}
	}
	
	@Override public void drawDebug(ShapeRenderer sr){
		sr.setColor(Color.YELLOW);
		super.drawDebug(sr);
	
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}
}
