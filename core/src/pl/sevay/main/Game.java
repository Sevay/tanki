package pl.sevay.main;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import pl.sevay.actors.Enemy;
import pl.sevay.managers.GameKeys;
import pl.sevay.managers.InputProcessor;
import pl.sevay.managers.Level;
import pl.sevay.managers.LevelManager;
import pl.sevay.actors.Bullet;
import pl.sevay.actors.Player;
import pl.sevay.actors.PowerUp;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import java.util.ArrayList;

public class Game implements ApplicationListener {
	
	public static int WIDTH;
	public static int HEIGHT;
	
	public static OrthographicCamera camera;
	
	Texture spriteSheet;
	
	SpriteBatch batch;
	ShapeRenderer sr;
	BitmapFont font;
	int time = 240;
	int point = 0;
	boolean won = false;
	public static Player player;
	LevelManager lvlManager;

	public static PowerUp powerUp;
	Texture powerUpSpriteSheet;
	
	int shootTimer;
	
	public void create () {
		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(WIDTH,HEIGHT);
		camera.setToOrtho(false, 416, 416);;
		camera.update();

		batch = new SpriteBatch();
		sr = new ShapeRenderer();
		spriteSheet = new Texture(Gdx.files.internal("TanksSpriteSheet.png"));
		powerUpSpriteSheet = new Texture(Gdx.files.internal("PowerUpSpriteSheet.png"));
		lvlManager = new LevelManager(spriteSheet);
		powerUp = new PowerUp(powerUpSpriteSheet, new Vector2(100, 100), 1, 3, 1, 0).colisionSetup();
		player = new Player(spriteSheet, Level.PLAYER_START_POS, 8, 8, 8, 0);

		shootTimer = 0;
		font = new BitmapFont();

		Gdx.input.setInputProcessor(new InputProcessor());
	}

	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		shootTimer++;
		if (GameKeys.isDown(GameKeys.LEFT)) {
			player.setVelocity(Player.LEFT);
		} else if (GameKeys.isDown(GameKeys.UP)) {
			player.setVelocity(Player.UP);
		} else if (GameKeys.isDown(GameKeys.RIGHT)) {
			player.setVelocity(Player.RIGHT);
		} else if (GameKeys.isDown(GameKeys.DOWN)) {
			player.setVelocity(Player.DOWN);
		} else {
			player.setVelocity(Player.STOPPED);
		}

		if (lvlManager.getCurrentLevel().resolveCollisions(player.getCollisionRect())) {
			player.setVelocity(Player.STOPPED);
		}

		for (int i = 0; i < lvlManager.getCurrentLevel().getEnemiesList().size(); i++) {
			for (int j = 0; j < lvlManager.getCurrentLevel().getEnemiesList().get(i).getBullets().size(); j++) {
				if (lvlManager.getCurrentLevel().getEnemiesList().get(i).getBullets().get(j).getCollisionRect().overlaps(player.getCollisionRect())) {
					lvlManager.getCurrentLevel().getEnemiesList().get(i).getBullets().get(j).setAlive(false);
					player.setAlive(false);
				}
			}
		}

		if (GameKeys.isDown(GameKeys.SPACE)) {
			if (shootTimer > 30) {
				player.shoot(Bullet.BULLET_PLAYER);
				shootTimer = 0;
			}
		}

		player.update(Gdx.graphics.getDeltaTime());
		powerUp.update(Gdx.graphics.getDeltaTime());

		for (int i = 0; i < player.getBullets().size(); i++) {
			if (lvlManager.getCurrentLevel().resloveDestructible(player.getBullets().get(i).getCollisionRect())) {
				player.getBullets().get(i).setAlive(false);
				continue;
			}
			if (lvlManager.getCurrentLevel().resloveEnemyCollisions(player.getBullets().get(i).getCollisionRect())) {
				player.getBullets().get(i).setAlive(false);
				point += 100;
				continue;
			}

			if (lvlManager.getCurrentLevel().resolveIronCollisions(player.getBullets().get(i).getCollisionRect())) {
				player.getBullets().get(i).setAlive(false);
				continue;
			}

			if (lvlManager.getCurrentLevel().resolveEagle(player.getBullets().get(i).getCollisionRect())) {
				player.getBullets().get(i).setAlive(false);
				lvlManager.getCurrentLevel().setEagleGetHit(true);
				continue;
			}
		}

		if (player.getCollisionRect().overlaps(powerUp.getCollisionRect()) && powerUp.isAlive()) {
			if(powerUp.getType() == PowerUp.BONUS) {
				point += 1000;
			} else if(powerUp.getType() == PowerUp.KILL_ALL_ENEMIES_ON_SCREEN) {
				point += 1000;
				for(Enemy e : lvlManager.getCurrentLevel().getEnemiesList()){
					e.setAlive(false);
				}
			} else if(powerUp.getType() == PowerUp.STOP_ENEMIES) {
				for(Enemy e : lvlManager.getCurrentLevel().getEnemiesList()){
					e.toggleStopTime();
				}
			}
			powerUp = powerUp.colisionSetup();
		}

		lvlManager.update(Gdx.graphics.getDeltaTime());

		GameKeys.update();

		batch.setProjectionMatrix(camera.combined);
		sr.setProjectionMatrix(camera.combined);

		lvlManager.drawLevelBack();

		batch.begin();
		player.draw(batch);
		lvlManager.draw(batch);
		if (powerUp.isAlive()) {
			powerUp.draw(batch);
		}
		powerUp.draw(batch);
		font.draw(batch, "Enemies left: " + lvlManager.getCurrentLevel().getEnemiesSumLeft(), 20, 390);
		font.draw(batch, "Score: " + point, 330, 390);
		if (!player.isAlive() || lvlManager.getCurrentLevel().isEagleGetHit() || won) {
			if (won) {
				font.draw(batch, "You won the game. Your score: " + point, 100, 200);
			} else {
				font.draw(batch, "GameOver. Your score: " + point, 150, 200);
			}

			if (time > 0) {
				time--;
			} else {
				Gdx.app.exit();
			}
		}
		//draw debug info
		font.draw(batch, String.valueOf(powerUp.time) + " " + powerUp.getPosition().toString() + " "
					+ player.getPosition().toString(), 30, 30);
		batch.end();

		sr.begin(ShapeType.Filled);
		player.drawDebug(sr);
		lvlManager.drawShapes(sr);
		sr.end();

		lvlManager.drawLevelFor();
		if (lvlManager.getCurrentLevel().getEnemiesSumLeft() == 0) {
			if (lvlManager.getCurrentLevelNumber() == 1) {
				won = true;
			} else {
				lvlManager.nextLevel();
				powerUp = powerUp.colisionSetup();
				player.setPosition(144, 32);
			}

		}
	}

	public void resize (int width, int height) {
	
	}

	public void pause () {
	
	}

	public void resume () {
	
	}

	public void dispose () {	
	
	}
}